from django.urls import path
from .views import show_recipe, show_list_page, create_recipe, edit_recipe

urlpatterns = [
    path("<int:id>/", show_recipe, name="show_individual_recipe"),
    path('list.html/', show_list_page, name="show_recipe_list"),
    path('create.html/', create_recipe, name="create_recipe"),
    path('<int:id>/edit/', edit_recipe, name="edit_recipe"),
]

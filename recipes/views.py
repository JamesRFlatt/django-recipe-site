from django.shortcuts import render, get_object_or_404, redirect
from .models import Recipe
from .forms import RecipeForm

def show_recipe(request, id):
    new_recipe = get_object_or_404(Recipe, id=id)
    context = {"recipe_object": new_recipe}

    return render(request, 'recipes/detail.html', context)

def show_list_page(request):
    print("a super long striiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiing", request)
    context = {"recipes": Recipe.objects.all}
    return render(request, 'recipes/list.html', context)

def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("show_recipe_list")
    else:
        form = RecipeForm()

    context = {
        "recipe_form": form
    }
    return render(request, "recipes/create.html/", context)

def edit_recipe(request, id):
    post = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance = post)
        if form.is_valid():
            form.save()
            return redirect("show_individual_recipe", id=id)
    else:
        form = RecipeForm(instance=post)

    context = {
        "recipe_form": form,
        "recipe_post": post,
    }

    return render(request, "recipes/edit.html/", context)
